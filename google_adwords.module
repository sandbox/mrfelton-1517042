<?php
/**
 * @file
 * Google Adwords Conversion Tracking Module
 */

// User permissions
define('GOOGLE_ADWORDS_ADMIN_PERM', 'administer google adwords module');
define('GOOGLE_ADWORDS_EDIT_PERM', 'edit google adwords parameters');

/**
 * Implementation of hook_permission().
 *
 * @todo Where do we need an permission check? Rules?
 */
function google_adwords_perm() {
  return array(GOOGLE_ADWORDS_ADMIN_PERM,
    GOOGLE_ADWORDS_ADMIN_PERM => array(
      'title' => t('Administer google adwords module.'),
      'description' => t('Perform administration tasks for google adwords.'),
    ),
    GOOGLE_ADWORDS_EDIT_PERM => array(
      'title' => t('Edit google adwords parameters.'),
      'description' => t('Edit google adwords parameters.'),
    ),
  );
}

/**
 * Implements hook_theme().
 */
function google_adwords_theme() {
  return array(
    'google_adwords_tracking' => array(
      'variables' => array(),
    ),
  );
}

/**
 * Implements hook_field_info().
 */
function google_adwords_field_info() {
  return array(
    'google_adwords_tracking' => array(
      'label' => 'Google Adwords Field',
      'description' => t('This field stores Google Adwords conversion tracking parameter.'),
      'default_widget' => 'text_textfield',
      'default_formatter' => 'default',
      'property_type' => 'text',
    ),
  );
}

/**
 * Implements hook_field_validate().
 */
function google_adwords_field_validate($obj_type, $object, $field, $instance, $langcode, $items, &$errors) {
  foreach ($items as $delta => $item) {
    if (!empty($item['conversion_id']) && (!is_numeric($item['conversion_id']) || (int) $item['conversion_id'] != $item['conversion_id'])) {
      $error = t('%name: The ID value has to be numeric.', array('%name' => $instance['label']));
      $errors[$field['field_name']][$langcode][$delta][] = array(
        'error' => $error,
        'message' => $error,
      );
    }
  }
  return;
}

/**
 * Implements hook_field_is_empty().
 */
function google_adwords_field_is_empty($item, $field) {
  // It makes no sense to store stuff without a conversion id.
  return empty($item['conversion_id']);
}

/**
 * Implements hook_field_formatter_info().
 */
function google_adwords_field_formatter_info() {
  $formats = array(
    'google_adwords_default' => array(
      'label' => t('Default'),
      'field types' => array('google_adwords_tracking'),
    ),
  );
  return $formats;
}

/**
 * Implements hook_field_formatter_view().
 */
function google_adwords_field_formatter_view($object_type, $object, $field, $instance, $langcode, $items, $display) {
  $element = array();

  foreach ($items as $delta => $item) {
    switch ($display['type']) {
      case 'google_adwords_default':
        $element[$delta]['#markup'] = theme('google_adwords_tracking', $item);
        break;
    }
  }
  return $element;
}

/**
 * Implements hook_field_widget_info().
 */
function google_adwords_field_widget_info() {
  return array(
    'google_adwords_tracking' => array(
      'label' => t('Google Adwords Tracking Field'),
      'field types' => array('google_adwords_tracking'),
    ),
  );
}

/**
 * Implements hook_field_presave().
 */
function google_adwords_field_presave($entity_type, $entity, $field, $instance, $langcode, &$items) {
  // Make sure nothing evil can bypass
  foreach ($items as $delta => &$item) {
    $item['conversion_id'] = (int) $item['conversion_id'];
    $item['label'] = filter_xss($item['label']);
    $item['value'] = filter_xss($item['value']);
    $item['language'] = filter_xss($item['language']);
    $item['color'] = filter_xss($item['color']);
    $item['format'] = filter_xss($item['format']);
  }
  return $items;
}

/**
 * Implements hook_field_widget_form().
 *
 * @todo Create a nicer form element.
 */
function google_adwords_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $base) {
  $fields = array(
    'conversion_id' => array('#title' => 'Conversion Id'),
    'label' => array('#title' => 'Label'),
    'value' => array('#title' => 'Value'),
    'language' => array('#title' => 'Language'),
    'color' => array('#title' => 'Color'),
    'format' => array('#title' => 'Format'),
  );

  $element = $base + array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  foreach ($fields as $field_name => $options) {
    $element[$field_name] = $options + array(
      '#type' => 'textfield',
      '#default_value' => isset($items[$delta][$field_name]) ? $items[$delta][$field_name] : NULL,
      '#size' => 60,
    );
  }

  return $element;
}

/**
 * Register a new conversion tracking.
 *
 * The rules integration calls this function.
 * Use this to set a custom tracking not related to a entity.
 *
 * @param integer $conversion_id
 * @param string $label
 * @param string $value
 * @param string $language
 * @param integer $color
 * @param integer $format
 */
function google_adwords_add_tracking($conversion_id, $label = NULL, $value = NULL, $language = NULL, $color = NULL, $format = NULL) {
  $cache = &drupal_static(__FUNCTION__, array());

  $label = ($label) ? $label : base64_encode($conversion_id);
  $language = ($language) ? $language : 'en';
  $color = ($color) ? $color : 'FFFFFF';
  $format = ($format) ? $format : 1;

  $cache[$conversion_id] = array(
    'conversion_id' => $conversion_id,
    'label' => $label,
    'language' => $language,
    'color' => $color,
    'format' => $format,
  	'value' => $value,
  );
}

/**
 * Get defined conversion trackings for this page.
 * Returns only the trackings set by rules or other modules using the function
 * google_adwords_add_tracking().
 *
 * @see google_adwords_add_tracking()
 *
 * @return array
 */
function google_adwords_get_tracking() {
  return drupal_static('google_adwords_add_tracking', array());
}

/**
 * Implements hook_page_build().
 * @param array $page
 *
 * @todo Do we need a setting to define in which area the code has to appear?
 */
function google_adwords_page_alter(&$page) {
  $trackings = google_adwords_get_tracking();
  foreach($trackings as $tracking) {
    $page['content']['system_main']['google_adwords'][$tracking['conversion_id']]['#markup'] = theme('google_adwords_tracking', $tracking);
  }
}

/**
 * Creates the code snipped for the tracking.
 * @param array $variables
 * @return markup
 */
function theme_google_adwords_tracking($variables){

  $value_js = NULL;
  $value_param = NULL;
  if (strlen($variables['value'])) {
    $value_js = '  var google_conversion_value = "' . $variables['value'] . '";' . PHP_EOL;
    $value_param = '&value=' . rawurlencode($variables['value']);
  }

  return 	PHP_EOL .
          '<!-- Google Code for Adwords Conversion Tracking -->' . PHP_EOL .
          '<script language="JavaScript" type="text/javascript">' . PHP_EOL .
          '<!--' . PHP_EOL .
          '  var google_conversion_id = ' . $variables['conversion_id'] . ';' . PHP_EOL .
          '  var google_conversion_language = "' . $variables['language'] . '";' . PHP_EOL .
          '  var google_conversion_format = "' . $variables['format'] . '";' . PHP_EOL .
          '  var google_conversion_color = "' . $variables['color'] . '";' . PHP_EOL .
          '  var google_conversion_label = "' . $variables['label'] . '";' . PHP_EOL .
          $value_js .
          '//-->' . PHP_EOL .
          '</script>' . PHP_EOL .
          '<script language="JavaScript" src="https://www.googleadservices.com/pagead/conversion.js">' . PHP_EOL .
          '</script>' . PHP_EOL .
          '<noscript>' . PHP_EOL .
          '  <img height=1 width=1 border=0 src="https://www.googleadservices.com/pagead/conversion/' . $variables['conversion_id'] . '/?script=0' . $value_param . '&label=' . $variables['label'] . '">' . PHP_EOL .
          '</noscript>' . PHP_EOL;
}
