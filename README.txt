
Module: Google AdWords Conversion Tracking
Author (D6): Jason Savino <http://drupal.org/user/411241>
Author (D7): Peter Philipp <http://drupal.org/user/762870>

Description
===========
Adds the Google AdWords Conversion Tracking to your nodes.

Requirements
============

* Google AdWrods user account


Installation
============
* Copy the 'google_adwords' module directory in to your Drupal
sites/all/modules directory as usual.


Usage (D6)
=====
In the settings page enter your Google AdWords account id,
default language, format, color and the location of the javascript
file used by Google AdWords.

You can also limit which user roles are tracked by ticking the 
appropriate role(s).

You enable the tracking of content-types in the content-type
edit form.
  
All pages will now have the required JavaScript added to the node.


Usage (D7)
=====
...